<?php
require('../config/database.php');

$postData = json_decode(file_get_contents('php://input'), true);

if(isset($postData['set_monitoring'])){

	$id_ruangan = $postData['id_ruangan'];
	$found = $postData['found'];
	$not_found = $postData['not_found'];
	$new = $postData['new'];
	$idStatusAktif = 1;

	$found_id = $database->select('barang', 
		[
			'barang.id_bluetooth',
			'barang.id_barang',
			'barang.id_status'
		],
		[
			'id_bluetooth'=> $found,
		]
	);

	foreach ($found_id as $f) {
		
		$database->insert('monitoring',[
			'waktu' => date('Y-m-d H:i:s'),
			'id_barang' => $f['id_barang'],
			'found' => true
		]);
	}

	$not_found_id = $database->select('barang', 
		[
			'barang.id_bluetooth',
			'barang.id_barang',
			'barang.id_status'
		],
		[
			'id_bluetooth'=> $not_found,
		]
	);

	foreach ($not_found_id as $nf) {
	

		$database->insert('monitoring',[
			'waktu' => date('Y-m-d H:i:s'),
			'id_barang' => $nf['id_barang'],
			'found' => false,
			
		]);	
		
	}

	$new_id = $database->select('barang', 
		[
			'barang.id_bluetooth',
			'barang.id_barang'
		],
		[
			'id_bluetooth'=> $new,
		]
	);

	foreach ($new_id as $n) {
		$database->update('barang',[
			'id_ruangan' => $id_ruangan,
		],[
			'id_barang' => $n['id_barang']
		]);

		$database->insert('monitoring',[
			'waktu' => date('Y-m-d H:i:s'),
			'id_barang' => $n['id_barang'],
			'found' => true
		]);
	}


	header('Content-Type: application/json;charset=utf-8');
	$data = [
		'success' => TRUE,
		'message' => 'Data berhasil diinput',
		'data' => []
	];
	echo json_encode($data);

}