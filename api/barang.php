<?php

require('../config/database.php');

$postData = json_decode(file_get_contents('php://input'), true);
if(isset($postData['get-barang'])){
	$id_ruangan = $postData['id_ruangan'];
	
	$barang = $database->select('barang', 
		[
			'barang.id_bluetooth',
			'barang.id_ruangan',
			'barang.id_status'
		],
		[
			'AND' => [
				'id_status'=>1,

				'id_ruangan'=>$id_ruangan
			]

		]
	);

	header('Content-Type: application/json;charset=utf-8');
	$data = [
		'success' => TRUE,
		'data' => $barang
	];
	echo json_encode($data);
}