<?php

include('component/com-barang.php');

?>

<section class="content-header">
	<h1><center>Daftar Inventaris Kantor<span class="small"></span></h1>
		<br>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-1">
		</div>
		<div class="col-md-2">
			<div class="small-box bg-blue">
				<div class="inner">
					<h3><center><?php echo $total_inventaris; ?></center></h3>
					<p><center>Total Inventaris</center></p>
				</div>
				<div class="icon">
					<i class=""></i>
				</div>
				<a class="small-box-footer" href="index.php?module=barang/inventaris-all">Lihat Selengkapnya</a>
			</div>
		</div>
		<div class="col-md-2">
			<div class="small-box bg-green">
				<div class="inner">
					<h3><center><?php echo $inventaris_aktif; ?></center></h3>
					<p><center>Inventaris Aktif</center></p>
				</div>
				<div class="icon">
					<i class=""></i>
				</div>
				<a class="small-box-footer" href="index.php?module=barang/inventaris-aktif">Lihat Selengkapnya</a>
			</div>
		</div>
		
		<div class="col-md-2">
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3><center><?php echo $inventaris_perbaikan; ?></center></h3>
					<p><center>Dalam Perbaikan</center></p>
				</div>
				<div class="icon">
					<i class=""></i>
				</div>
				<a class="small-box-footer" href="index.php?module=barang/inventaris-perbaikan">Lihat Selengkapnya</a>
			</div>
		</div>
		<div class="col-md-2">
			<div class="small-box bg-red">
				<div class="inner">
					<h3><center><?php echo $inventaris_rusak; ?></center></h3>
					<p><center>Inventaris Rusak</center></p>
				</div>
				<div class="icon">
					<i class=""></i>
				</div>
				<a class="small-box-footer" href="index.php?module=barang/inventaris-rusak">Lihat Selengkapnya</a>
			</div>
		</div>
		<div class="col-md-2">
			<div class="small-box bg-red">
				<div class="inner">
					<h3><center><?php echo $inventaris_hilang; ?></center></h3>
					<p><center>Inventaris Hilang</center></p>
				</div>
				<div class="icon">
					<i class=""></i>
				</div>
				<a class="small-box-footer" href="index.php?module=barang/inventaris-hilang">Lihat Selengkapnya</a>
				</div>
			</div>
		</div>	
	<section class="content-header">
	<h1>Inventaris Kantor<span class="small">   </span></h1>
</section>
<section class="content">
	<div class="box">
		
		<div class="box-body">
			<table class="table table-striped" id='monitoring-table-2'>
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Barang</th>
						<th>Ruangan</th>
						<th>Merek Barang</th>
						<th>Nomor Inventaris</th>
						<th>ID Bluetooth</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$no=1;
					foreach ($barang as $barang) { ?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo $barang['nama_barang']; ?></td>
						<td><?php echo $barang['nama_ruangan']; ?></td>
						<td><?php echo $barang['merek_barang']; ?></td>
						<td><?php echo $barang['nomor_inventaris']; ?></td>
						<td><?php echo $barang['id_bluetooth']; ?></td>
						<td><?php echo $barang['nama_status']; ?></td>
						<td>
							<a href="?module=barang/barang-update&barang=<?php echo $barang['id_barang']; ?>" class="btn btn-xs btn-info">Update</a>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
			
		</div>
	</div>
</section>