<?php
include('component/com-barang.php');
?>

<section class="content-header">
	<h1>Inventaris <span class="small">Tambah data barang baru</span></h1>
</section>

<section class="content">
	<?php if(isset($_POST['barang-add'])) { ?>
	<div class="alert alert-success">
		<h4>Berhasil</h4>
		Anda berhasil melakukan penambahan barang. 
		<a href="?module=barang/barang-add">Tambah barang lagi</a> / 
		<a href="?module=barang/barang-list">Kembali</a>
	</div>
	<?php } else { ?>
	<form action="" method="post">
		<div class="box">
			<div class="box-body">
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label>Nama Barang</label>
							<input class="form-control" name="nama_barang" required />
						</div>
						<div class="form-group">
							<label>Merek Barang</label>
							<input class="form-control" name="merek_barang" required />
						</div>
						<div class="form-group">
							<label>ID Bluetooth / MAC Address</label>
							<input class="form-control" name="id_bluetooth" required />
						
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label>Nomor Inventaris</label>
							<input class="form-control" name="nomor_inventaris" required />
						</div>
						<div class="form-group">
							<label>Ruangan</label>
							<select class="form-control" name="id_ruangan">
								<option>-- Pilih --</option>
								<?php foreach ($ruangan as $ruangan) { ?>
								<option value="<?php echo $ruangan['id_ruangan']; ?>"><?php echo $ruangan['nama_ruangan']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<label>Status</label>
							<select class="form-control" name="id_status">
								<option>-- Pilih --</option>
								<?php foreach ($status as $status) { ?>
								<option value="<?php echo $status['id_status']; ?>"><?php echo $status['nama_status']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="box-footer">
				<button class="btn btn-success" type="submit" name="barang-add">Tambah Barang</button>
				<a class="btn btn-warning" href="?module=barang/barang-list">Batal</a>
			</div>
		</div>
	</form>
	<?php } ?>
</content>