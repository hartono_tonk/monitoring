<?php

include('component/com-barang.php');

?>
<section class="content-header">
	<h1>Daftar Inventaris Hilang<span class="small"></span></h1>
</section>

<section class="content">
	<div class="box">
		
		<div class="box-body">
			<table class="table table-striped" id='monitoring-table-2'>
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Barang</th>
						<th>Merek Barang</th>
						<th>Nomor Inventaris</th>
						<th>ID Bluetooth</th>
						<th>Status</th>
						<th>Action</th>
						
					</tr>
				</thead>
				<tbody>
					<?php 
					$no=1;
					foreach ($status_hilang as $barang) { ?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo $barang['nama_barang']; ?></td>
						<td><?php echo $barang['merek_barang']; ?></td>
						<td><?php echo $barang['nomor_inventaris']; ?></td>
						<td><?php echo $barang['id_bluetooth']; ?></td>
						<td><?php echo $barang['nama_status']; ?></td>
						<td><a href="?module=barang/hilang-update&barang=<?php echo $barang['id_barang']; ?>" class="btn btn-xs btn-info">Update</a>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
			<div class="box-footer">
				<p><i>Note: Tidak ditemukan dalam 10 kali scan oleh sistem</i></p>
				<a class="btn btn-primary btn-sm" href="index.php">Kembali Ke Dashboard Admin</a>
			</div>
		</div>
		</div>
	</div>
</section>