<?php

include('component/com-barang.php');

?>

<section class="content-header">
	<h1>Hapus Barang</h1>
</section>

<section class="content">
	<?php if(isset($_POST['barang-del'])) { ?>
	<div class="alert alert-success">
		<h4>Berhasil</h4>
		Anda telah sukses melakukan penghapusan data barang. <b><a href="?module=barang/barang-list">Kembali</a></b>
	</div>
	<?php } else {?>
	<div class="alert alert-warning">
		<h4>Peringatan</h4>
		Apakah anda yakin untuk menghapus barang : <?php echo $barang_view['nama_barang']; ?> ? (data yang sudah dihapus tidak dapat dikembalikan lagi)
		<br/><br/>
		<form action="" method="post">
			<input type="hidden" name="id_barang" value="<?php echo $barang_view['id_barang']; ?>" />
			<button class="btn btn-success" type="submit" name="barang-del">Ya! Hapus</button>
			<a class="btn btn-info" href="?module=barang/barang-update&barang=<?php echo $barang_view['id_barang']; ?>">Batal</a>
		</form>
	</div>
	<?php } ?>
</section>