<?php

include('component/com-barang.php');

?>
<section class="content-header">
	<h1>Daftar Alat Tidak Terdeteksi<span class="small"></span></h1>
</section>

<section class="content">
	<div class="box">
		
		<div class="box-body">
			<table class="table table-striped" id='monitoring-table-2'>
				<thead>
					<tr>
						<th>No.</th>
						<th>ID Barang</th>
						<th>ID Bluetooth</th>
						<th>Nama Barang</th>
						<th>Nomor Inventaris</th>
						<th>Nama Ruangan</th>
						<th>Total Not Found</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$no=1;
					foreach ($res as $barang) { ?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo $barang['id_barang']; ?></td>
						<td><?php echo $barang['id_bluetooth']; ?></td>
						<td><?php echo $barang['nama_barang']; ?></td>
						<td><?php echo $barang['nomor_inventaris']; ?></td>
						<td><?php echo $barang['nama_ruangan'];?></td>
						<td><?php echo $barang['not_found']; ?></td>
						<td>
							<a href="?module=barang/barang-warning-update&barang=<?php echo $barang['id_barang']; ?>" class="btn btn-xs btn-info">Update</a>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
			<div class="box-footer">
				<a class="btn btn-primary btn-sm" href="index.php">Kembali Ke Dashboard Admin</a>
			</div>
		</div>
		</div>
	</div>
</section>