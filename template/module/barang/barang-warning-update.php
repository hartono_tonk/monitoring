<?php

include('component/com-barang.php');

?>

<section class="content-header">
	<h1>Edit Barang <span class="small"></span></h1>
</section>

<section class="content">
	<?php if(isset($_POST['barang-update'])) { ?>
	<div class="alert alert-success">
		<h4>Berhasil</h4>
		Anda berhasil melakukan perubahan barang. 
		<a href="index.php?module=monitoring/monitoring-list">Kembali</a>
	</div>
	<?php } else { ?>
	<form action="" method="post">
		<div class="box">
			<div class="box-body">
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label>Nama Barang</label>
							<input class="form-control" name="nama_barang" value="<?php echo $barang_view['nama_barang']; ?>" readonly />
						</div>
						<div class="form-group">
							<label>Merek Barang</label>
							<input class="form-control" name="merek_barang" value="<?php echo $barang_view['merek_barang']; ?>"  readonly />
						</div>
						<div class="form-group">
							<label>ID Bluetooth / MAC Address</label>
							<input class="form-control" name="id_bluetooth" value="<?php echo $barang_view['id_bluetooth']; ?>"readonly />
						
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label>Nomor Inventaris</label>
							<input class="form-control" name="nomor_inventaris" value="<?php echo $barang_view['nomor_inventaris']; ?>"readonly />
						</div>
						<div class="form-group">
							<label>Ruangan</label>
							<select class="form-control" name="id_ruangan">
								<option>-- Pilih --</option>
								<?php foreach ($ruangan as $ruangan) { ?>
								<option value="<?php echo $ruangan['id_ruangan']; ?>" <?php echo $barang_view['id_ruangan'] == $ruangan['id_ruangan'] ? 'selected' : null ?>><?php echo $ruangan['nama_ruangan']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<label>Status</label>
							<select class="form-control" name="id_status">
								<?php foreach ($status as $status) { ?>
								<option value="<?php echo $status['id_status']; ?>" <?php echo $barang_view['id_status'] == $status['id_status'] ? 'selected' : null ?>><?php echo $status['nama_status']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="box-footer">
				
				<input type="hidden" name="id_barang" value="<?php echo $barang_view['id_barang']; ?>" />
				<button class="btn btn-success" type="submit" name="barang-update">Update Barang</button>
				<a class="btn btn-danger" href="?module=barang/barang-delete&barang=<?php echo $barang_view['id_barang']; ?>">Hapus Barang</a>
				<a class="btn btn-warning" href="?module=barang/barang-list">Batal</a>
			</div>
	</form>
	<?php } ?>
</content>