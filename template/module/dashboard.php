<meta http-equiv="refresh" content="30">
<?php

include('component/com-barang.php');

include('component/com-monitoring.php');

?>

<section class="content-header">
	<h1>Dashboard <span class="small"></span></h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-sm-3">
			<div class="small-box bg-green">
				<div class="inner">
					<h3><?php echo $inventaris_aktif; ?></h3>
					<p>Inventaris Aktif</p>
				</div>
				<div class="icon">
					<i class=""></i>
				</div>
				<a class="small-box-footer" href="index.php?module=barang/inventaris-aktif">Lihat Selengkapnya</a>
			</div>
		</div>
		
		<div class="col-sm-3">
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3><?php echo $inventaris_perbaikan; ?></h3>
					<p>Inventaris Dalam Perbaikan</p>
				</div>
				<div class="icon">
					<i class=""></i>
				</div>
				<a class="small-box-footer" href="index.php?module=barang/inventaris-perbaikan">Lihat Selengkapnya</a>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="small-box bg-red">
				<div class="inner">
					<h3><?php echo $inventaris_rusak; ?></h3>
					<p>Inventaris Rusak</p>
				</div>
				<div class="icon">
					<i class=""></i>
				</div>
				<a class="small-box-footer" href="index.php?module=barang/inventaris-rusak">Lihat Selengkapnya</a>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="small-box bg-red">
				<div class="inner">
					<h3><?php echo $inventaris_hilang; ?></h3>
					<p>Inventaris Hilang</p>
				</div>
				<div class="icon">
					<i class=""></i>
				</div>
				<a class="small-box-footer" href="index.php?module=barang/inventaris-hilang">Lihat Selengkapnya</a>
			</div>
		</div>
	</div>	
	<section class="content-header">
		<h1>Monitoring<span class="small">   Inventaris Kantor</span></h1>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-body">
				<div class="table-responsive">
					<table class="table" id="monitoring-table">
						<thead>
							<tr>
								<th>ID No.</th>
								<th>Date & Time</th>
								<th>Nama Barang</th>
								<th>ID Bluetooth</th>
								<th>Nomor Inventaris</th>
								<th>Ruangan</th>
								<th>Terdeteksi</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							
							foreach ($monitoring_dashboard as $monitoring_dashboard) { ?>
								<tr>
									<td><?php echo $monitoring_dashboard['id_monitoring']; ?></td>
									<td><?php echo $monitoring_dashboard['waktu']; ?></td>	
									<td><?php echo $monitoring_dashboard['nama_barang']; ?></td>
									<td><?php echo $monitoring_dashboard['id_bluetooth']; ?></td>
									<td><?php echo $monitoring_dashboard['nomor_inventaris']; ?></td>
									<td><?php echo $monitoring_dashboard['nama_ruangan']; ?></td>
									<td><?php echo $monitoring_dashboard['found'] ? 'Ya' : 'Tidak'; ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</section>