<?php
include('component/com-ruangan.php');
?>

<section class="content-header">
	<h1>Ruangan <span class="small">Tambah data ruangan baru</span></h1>
</section>

<section class="content">
	<?php if(isset($_POST['ruangan-add'])) { ?>
	<div class="alert alert-success">
		<h4>Berhasil</h4>
		Anda berhasil melakukan penambahan ruangan. 
		<a href="?module=ruangan/ruangan-add">Tambah ruangan lagi</a> / 
		<a href="?module=ruangan/ruangan-list">Kembali</a>
	</div>
	<?php } else { ?>
	<form action="" method="post">
		<div class="box">
			<div class="box-body">
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label>Nama Ruangan</label>
							<input class="form-control" name="nama_ruangan" required />
						</div>
						<div class="form-group">
							<label>Nama Device Sacanner</label>
							<select class="form-control" name="id_device">
								<option>-- Pilih --</option>
								<?php foreach ($ruangan_device as $ruangan_device) { ?>
								<option value="<?php echo $ruangan_device['id_device']; ?>"><?php echo $ruangan_device['nama_device']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					
			</div>
		</div>
		<div class="box-footer">
				<button class="btn btn-success" type="submit" name="ruangan-add">Tambah Ruangan</button>
				<a class="btn btn-warning" href="?module=ruangan/ruangan-list">Batal</a>
			</div>
		</div>
	</form>
	<?php } ?>
</content>