<?php

include('component/com-ruangan.php');

?>

<section class="content-header">
	<h1>Hapus ruangan</h1>
</section>

<section class="content">
	<?php if(isset($_POST['ruangan-del'])) { ?>
		<div class="alert alert-success">
			<h4>Berhasil</h4>
			Anda telah sukses melakukan penghapusan data ruangan. <b><a href="?module=ruangan/ruangan-list">Kembali</a></b>
		</div>
	<?php } else {?>
		<div class="alert alert-warning">
			<h4>Peringatan</h4>
			Apakah anda yakin untuk menghapus ruangan <?php echo $ruangan_view['nama_ruangan']; ?> ? (data yang sudah dihapus tidak dapat dikembalikan lagi)
			<br/><br/>
			<form action="" method="post">
				<input type="hidden" name="id_ruangan" value="<?php echo $ruangan_view['id_ruangan']; ?>" />
				<button class="btn btn-success" type="submit" name="ruangan-del">Ya! Hapus</button>
				<a class="btn btn-info" href="?module=ruangan/ruangan-update&ruangan=<?php echo $ruangan_view['id_ruangan']; ?>">Batal</a>
			</form>
		</div>
	<?php } ?>
</section>