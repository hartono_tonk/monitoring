<?php

include('component/com-ruangan.php');

?>
<section class="content-header">
	<h1>Ruangan<span class="small">Daftar Ruang Kantor</span></h1>
</section>

<section class="content">
	<div class="box">
		
		<div class="box-body">
			<table class="table table-striped table-hover" id='monitoring-table-2'>
				<thead>
					<tr>
						<th>No.</th>
						<th>ID RUangan</th>
						<th>Nama Ruangan</th>
						<th>Nama Device Sacanner</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$no=1;
					foreach ($ruangan as $ruangan) { ?>
						<tr>
							<td><?php echo $no++; ?></td>
							<td align=""><?php echo $ruangan['id_ruangan']; ?></td>
							<td><?php echo $ruangan['nama_ruangan']; ?></td>
							<td><?php echo $ruangan['nama_device']; ?></td>
							<td>
								<a href="?module=ruangan/ruangan-update&ruangan=<?php echo $ruangan['id_ruangan']; ?>" class="btn btn-xs btn-info">Update</a>
							</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
			
		</div>
	</div>
</section>