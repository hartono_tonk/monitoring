<?php

include('component/com-ruangan.php');

?>

<section class="content-header">
	<h1>Edit Barang <span class="small"></span></h1>
</section>

<section class="content">
	<?php if(isset($_POST['ruangan-update'])) { ?>
		<div class="alert alert-success">
			<h4>Berhasil</h4>
			Anda berhasil melakukan perubahan barang. 
			<a href="?module=ruangan/ruangan-list">Kembali</a>
		</div>
	<?php } else { ?>
		<form action="" method="post">
			<div class="box">
				<div class="box-body">
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label>Nama Ruangan</label>
								<input class="form-control" name="nama_ruangan" value="<?php echo $ruangan_view['nama_ruangan']; ?>" required />
							</div>
							<div class="form-group">
								<label>Nama Device Sacanner</label>
								<select class="form-control" name="id_device">
									<option><?php echo $ruangan_view['nama_device']; ?></option>
									<?php foreach ($ruangan_device as $ruangan_device) { ?>
										<option value="<?php echo $ruangan_device['id_device']; ?>"><?php echo $ruangan_device['nama_device']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						
						
					</div>
					<div class="box-footer">
						<input type="hidden" name="id_ruangan" value="<?php echo $ruangan_view['id_ruangan']; ?>" />
						<button class="btn btn-success" type="submit" name="ruangan-update">Update Ruangan</button>
						<a class="btn btn-danger" href="?module=ruangan/ruangan-delete&ruangan=<?php echo $ruangan_view['id_ruangan']; ?>">Hapus Ruangan</a>
						<a class="btn btn-warning" href="?module=ruangan/ruangan-list">Batal</a>
					</div>
				</div>
			</form>
		<?php } ?>
	</content>