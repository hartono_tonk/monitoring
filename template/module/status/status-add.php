<?php
include('component/com-status.php');
?>

<section class="content-header">
	<h1>STATUS <span class="small">Tambah data status baru</span></h1>
</section>

<section class="content">
	<?php if(isset($_POST['status-add'])) { ?>
		<div class="alert alert-success">
			<h4>Berhasil</h4>
			Anda berhasil melakukan penambahan status barang. 
			<a href="?module=status/status-add">Tambah status lagi</a> / 
			<a href="?module=status/status-list">Kembali</a>
		</div>
	<?php } else { ?>
		<form action="" method="post">
			<div class="box">
				<div class="box-body">
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label>Nama Status</label>
								<input class="form-control" name="nama_status" required />
							</div>
						</div>	
					</div>
				</div>
				<div class="box-footer">
					<button class="btn btn-success" type="submit" name="status-add">Tambah Status</button>
					<a class="btn btn-warning" href="?module=status/status-list">Batal</a>
				</div>
			</div>
		</form>
	<?php } ?>
</content>