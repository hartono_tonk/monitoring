<?php

include('component/com-status.php');

?>

<section class="content-header">
	<h1>Edit Status Barang <span class="small"></span></h1>
</section>

<section class="content">
	<?php if(isset($_POST['status-update'])) { ?>
		<div class="alert alert-success">
			<h4>Berhasil</h4>
			Anda berhasil melakukan perubahan status. 
			<a href="?module=status/status-list">Kembali</a>
		</div>
	<?php } else { ?>
		<form action="" method="post">
			<div class="box">
				<div class="box-body">
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label>Nama Status</label>
								<input class="form-control" name="nama_status" value="<?php echo $status_view['nama_status']; ?>"required />
							</div>
						</div>
						
						
					</div>
					<div class="box-footer">
						<input type="hidden" name="id_status" value="<?php echo $status_view['id_status']; ?>" />
						<button class="btn btn-success" type="submit" name="status-update">Update Status</button>
						<a class="btn btn-danger" href="?module=status/status-delete&status=<?php echo $status_view['id_status']; ?>">Hapus Status</a>
						<a class="btn btn-warning" href="?module=status/status-list">Batal</a>
					</div>
				</div>
			</form>
		<?php } ?>
	</content>