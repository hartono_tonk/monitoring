<?php

include('component/com-status.php');

?>
<section class="content-header">
	<h1>Status Barang<span class="small"></span></h1>
</section>

<section class="content">
	<div class="box">
		<div class="box-header">
			<a class="btn btn-info" href="?module=status/status-add">Tambah Status</a>
		</div>
		<div class="box-body">
			<table class="table table-striped" id='monitoring-table-2'>
				<thead>
					<tr>
						<th>No.</th>
						<th>ID Status</th>
						<th>Nama Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$no=1;
					foreach ($status as $status) { ?>
						<tr>
							<td><?php echo $no++; ?></td>
							<td><?php echo $status['id_status']; ?></td>
							<td><?php echo $status['nama_status']; ?></td>
							<td>
								<a href="?module=status/status-update&status=<?php echo $status['id_status']; ?>" class="btn btn-xs btn-info">Update</a>
							</td>
						</tr>
					<?php  } ?>
				</tbody>
			</table>
			
		</div>
	</div>
</section>