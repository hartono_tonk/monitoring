<?php

include('component/com-status.php');

?>

<section class="content-header">
	<h1>Hapus Status</h1>
</section>

<section class="content">
	<?php if(isset($_POST['status-del'])) { ?>
		<div class="alert alert-success">
			<h4>Berhasil</h4>
			Anda telah sukses melakukan penghapusan data status. <b><a href="?module=status/status-list">Kembali</a></b>
		</div>
	<?php } else {?>
		<div class="alert alert-warning">
			<h4>Peringatan</h4>
			Apakah anda yakin untuk menghapus data STATUS <?php echo $status_view['nama_status']; ?> ? (data yang sudah dihapus tidak dapat dikembalikan lagi)
			<br/><br/>
			<form action="" method="post">
				<input type="hidden" name="id_status" value="<?php echo $status_view['id_status']; ?>" />
				<button class="btn btn-success" type="submit" name="status-del">Ya! Hapus</button>
				<a class="btn btn-info" href="?module=status/status-update&status=<?php echo $status_view['id_status']; ?>">Batal</a>
			</form>
		</div>
	<?php } ?>
</section>