<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="template/images/user.png" class="img-circle" alt="User Image" />
      </div>
      <div class="pull-left info">
        <p><?php echo $_SESSION['username']; ?></p>
        <span class="small"><?php echo date('l. d M Y'); ?></span>
      </div>
    </div>

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li>
        <a href="index.php">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      
      
      <?php if($_SESSION['batasan']=1) { ?>
        <li class="header">MENU</li>
        <li class="treeview">
          <a href="#">
            <i class=""></i>
            <span>Barang</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="?module=barang/barang-list"><i class="fa fa-circle-o"></i> Lihat Barang</a></li>
            <li><a href="?module=barang/barang-add"><i class="fa fa-circle-o"></i> Tambah Barang</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class=""></i>
            <span>Ruangan</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="?module=ruangan/ruangan-list"><i class="fa fa-circle-o"></i> Lihat Ruangan</a></li>
            <li><a href="?module=ruangan/ruangan-add"><i class="fa fa-circle-o"></i> Tambah Ruangan</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class=""></i>
            <span>Monitoring</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="?module=monitoring/monitoring-list"><i class="fa fa-circle-o"></i> Monitoring Barang</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class=""></i>
            <span>Setting</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="?module=status/status-list"><i class="fa fa-circle-o"></i> Setting Status</a></li>
            <li><a href="?module=device/device-list"><i class="fa fa-circle-o"></i> Setting Device</a></li>
          </ul>
        </li>
        
        
        
        <?php if($_SESSION['batasan']==1) { ?>
          
         
        <?php } } ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>