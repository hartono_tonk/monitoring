<head>
    <meta charset="UTF-8">

    <link rel="shortcut icon" href="template/images/ble.ico">
    <title>Monitoring-BLE</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="template/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="template/bootstrap/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Plugin Styl -->
    <link href="template/plugins/select2/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="template/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="template/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
       <link href="template/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

       <link rel="stylesheet" type="text/css" href="template/plugins/datatables/jquery.dataTables.min.css">
</head>