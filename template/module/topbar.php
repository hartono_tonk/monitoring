<header class="main-header">
  <!-- Logo -->
  <a href="index.php" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>M</b></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>Monitoring</b></span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </a>
    <a href="#" class="navbar-brand">
      <b>Sistem Monitoring     -</b>   
      <span class="small">PT. Gemilang Oetama Karya</span>
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->
        
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="template/images/user.png" class="user-image" alt="User Image" />
            <span class="hidden-xs"><?php echo $_SESSION['username']; ?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="template/images/user.png  ?>" class="img-circle" alt="User Image" />
              <p>
                <?php echo $_SESSION['username']; ?>
                
              </p>
            </li>               
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-right">
                <a href="logout.php" class="btn btn-default btn-flat">Log Out</a>
              </div>
            </li>
          </ul>
        </li>
        <!-- Control Sidebar Toggle Button -->
      </ul>
    </div>
  </nav>
</header>