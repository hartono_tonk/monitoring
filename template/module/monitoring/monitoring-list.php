<meta http-equiv="refresh" content="30">
<?php

include('component/com-barang.php');

include('component/com-monitoring.php');

?>

<section class="content">
	<div class="box">
		<div class="box-body">
			<div class="alert alert-danger">
				<div class="text-left">
						<font size="15">Alat Tidak Terdeteksi</font><br>
						<?php 
						$no=1;
						foreach ($res as $hitung) {
								if ($hitung['not_found']==10 && $hitung['id_barang']==$hitung['id_barang'] && $hitung['id_status']==1) {
									echo $no++;
									echo ". ".$hitung['nama_barang']."<br>";
								}
						}?>	
					<div class="">
						<a class="small-box-footer" href="index.php?module=barang/inventaris-warning">Lihat Selengkapnya</a>
					</div>
				</div>		
			</div>
		</div>
			<div class="table-responsive">
			<table class="table" id="monitoring-table">
				<thead>
					<tr>
						<th>No. ID</th>
						<th>Date & Time</th>
						<th>Ruangan</th>
						<th>Terdeteksi</th>
						<th>Nama Barang</th>
						<th>Merek</th>
						<th>ID Bluetooth</th>
						<th>Nomor Inventaris</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$no=1;
					foreach ($monitorings as $monitoring) { ?>
					<tr>
						<td><?php echo $monitoring['id_monitoring']; ?></td>
						<td><?php echo $monitoring['waktu']; ?></td>
						<td><?php echo $monitoring['nama_ruangan']; ?></td>
						<td><?php echo $monitoring['found'] ? 'Ya' : 'Tidak'; ?></td>
						<td><?php echo $monitoring['nama_barang']; ?></td>
						<td><?php echo $monitoring['merek_barang']; ?></td>
						<td><?php echo $monitoring['id_bluetooth']; ?></td>
						<td><?php echo $monitoring['nomor_inventaris']; ?></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</section>
