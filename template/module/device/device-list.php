<?php

include('component/com-device.php');

?>
<section class="content-header">
	<h1>Device <span class="small"> Bluetooth Scanner Device</span></h1>
</section>

<section class="content">
	<div class="box">
		<div class="box-header">
			<a class="btn btn-info" href="?module=device/device-add">Tambah Device</a>
		</div>
		<div class="box-body">
			<table class="table table-striped" id='monitoring-table-2'>
				<thead>
					<tr>
						<th>No.</th>
						<th>ID Device</th>
						<th>Nama Device</th>
						<th>MAC Address</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$no=1;
					foreach ($device as $device) { ?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo $device['id_device']; ?></td>
						<td><?php echo $device['nama_device']; ?></td>
						<td><?php echo $device['mac_address']; ?></td>						
						<td>
							<a href="?module=device/device-update&device=<?php echo $device['id_device']; ?>" class="btn btn-xs btn-info">Update</a>
						</td>
					</tr>
					<?php  } ?>
				</tbody>
			</table>
			
		</div>
	</div>
</section>