<?php

include('component/com-device.php');

?>

<section class="content-header">
	<h1>Hapus device</h1>
</section>

<section class="content">
	<?php if(isset($_POST['device-del'])) { ?>
	<div class="alert alert-success">
		<h4>Berhasil</h4>
		Anda telah sukses melakukan penghapusan data device. <b><a href="?module=device/device-list">Kembali</a></b>
	</div>
	<?php } else {?>
	<div class="alert alert-warning">
		<h4>Peringatan</h4>
		Apakah anda yakin untuk menghapus Nama Device <?php echo $device_view['nama_device']; ?> ? (data yang sudah dihapus tidak dapat dikembalikan lagi)
		<br/><br/>
		<form action="" method="post">
			<input type="hidden" name="id_device" value="<?php echo $device_view['id_device']; ?>" />
			<button class="btn btn-success" type="submit" name="device-del">Ya! Hapus</button>
			<a class="btn btn-info" href="?module=device/device-update&device=<?php echo $device_view['id_device']; ?>">Batal</a>
		</form>
	</div>
	<?php } ?>
</section>