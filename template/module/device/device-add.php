<?php
include('component/com-device.php');
?>

<section class="content-header">
	<h1>DEVICE <span class="small">Tambah data device baru</span></h1>
</section>

<section class="content">
	<?php if(isset($_POST['device-add'])) { ?>
	<div class="alert alert-success">
		<h4>Berhasil</h4>
		Anda berhasil melakukan penambahan device scanner baru. 
		<a href="?module=device/device-add">Tambah device lagi</a> / 
		<a href="?module=device/device-list">Kembali</a>
	</div>
	<?php } else { ?>
	<form action="" method="post">
		<div class="box">
			<div class="box-body">
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label>Nama Device</label>
							<input class="form-control" name="nama_device" required />
						</div>
						<div class="form-group">
							<label>Device MAC Address</label>
							<input class="form-control" name="mac_address" required />
						</div>
					</div>	
				</div>
			</div>
		<div class="box-footer">
				<button class="btn btn-success" type="submit" name="device-add">Tambah Device</button>
				<a class="btn btn-warning" href="?module=device/device-list">Batal</a>
			</div>
		</div>
	</form>
	<?php } ?>
</content>