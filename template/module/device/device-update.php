<?php

include('component/com-device.php');

?>

<section class="content-header">
	<h1>Edit Device Bluetooth Scanner <span class="small"></span></h1>
</section>

<section class="content">
	<?php if(isset($_POST['device-update'])) { ?>
	<div class="alert alert-success">
		<h4>Berhasil</h4>
		Anda berhasil melakukan perubahan device. 
		<a href="?module=device/device-list">Kembali</a>
	</div>
	<?php } else { ?>
	<form action="" method="post">
		<div class="box">
			<div class="box-body">
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label>Nama Device</label>
							<input class="form-control" name="nama_device" value="<?php echo $device_view['nama_device']; ?>"required />
						</div>
						<div class="form-group">
							<label>Device MAC Address</label>
							<input class="form-control" name="mac_address" value="<?php echo $device_view['mac_address']; ?>"required />
						</div>
					</div>
					
					
			</div>
			<div class="box-footer">
				<input type="hidden" name="id_device" value="<?php echo $device_view['id_device']; ?>" />
				<button class="btn btn-success" type="submit" name="device-update">Update Device</button>
				<a class="btn btn-danger" href="?module=device/device-delete&device=<?php echo $device_view['id_device']; ?>">Hapus Device</a>
				<a class="btn btn-warning" href="?module=device/device-list">Batal</a>
			</div>
		</div>
	</form>
	<?php } ?>
</content>