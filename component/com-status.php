<?php

// List & View status
$status=$database->select('status','*');

//insert
if(isset($_POST['status-add'])){

	$database->insert('status',[
		'nama_status'=>$_POST['nama_status']
		]);
}

// vie by id status
if(!empty($_GET['status'])){

	$status_view=$database->get('status',[
		'id_status',
		'nama_status'
		],[
		'id_status'=>$_GET['status'],
		]);
}

//Update status
if(isset($_POST['status-update'])){

	$database->update('status',[
		'nama_status'=>$_POST['nama_status']],
		[
		'id_status'=>$_POST['id_status']
		]);
	
}

//Delete status
if(isset($_POST['status-del'])){

	$database->delete('status',['id_status'=>$_POST['id_status']]);

}
?>