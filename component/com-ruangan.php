<?php

// List & View barang
		
$ruangan=$database->select('ruangan',[
	'[><]device'=>'id_device'],
	[
		'ruangan.id_ruangan',
		'ruangan.nama_ruangan',
		'device.id_device',
		'device.nama_device'
	]);


// Tambah Ruangan
if(isset($_POST['ruangan-add'])){

	$database->insert('ruangan',[
		'nama_ruangan'=>$_POST['nama_ruangan'],
		'id_device'=>$_POST['id_device']
		]);
}

// get ruangan
if(!empty($_GET['ruangan'])) {

	$ruangan_view=$database->get('ruangan',[
		'[><]device'=>'id_device'
		],[
		'ruangan.id_ruangan',
		'ruangan.nama_ruangan',
		'device.id_device',
		'device.nama_device'
		],[
		'id_ruangan'=>$_GET['ruangan']
		]);
}

//Update barang
if(isset($_POST['ruangan-update'])){

	$database->update('ruangan',[
		'nama_ruangan'=>$_POST['nama_ruangan'],
		'id_device'=>$_POST['id_device']
		],[
		'id_ruangan'=>$_POST['id_ruangan']
		]);
	
}



//Delete barang
if(isset($_POST['ruangan-del'])){

	$database->delete('ruangan',['id_ruangan'=>$_POST['id_ruangan']]);

}


$ruangan_device=$database->select('device','*');

if(!empty($_GET['ruangan_device'])){

	$ruangan_device_view=$database->get('device','*',['nama_device'=>$_GET['ruangan_device']]);
}


?>