<?php

// List & View device
$device=$database->select('device','*');

//insert
if(isset($_POST['device-add'])){

	$database->insert('device',[
		'nama_device'=>$_POST['nama_device'],
		'mac_address'=>$_POST['mac_address']
		]);
}

// vie by id device
if(!empty($_GET['device'])){

	$device_view=$database->get('device',[
		'id_device',
		'nama_device',
		'mac_address'
		],[
		'id_device'=>$_GET['device'],
		]);
}

//Update device
if(isset($_POST['device-update'])){

	$database->update('device',[
		'nama_device'=>$_POST['nama_device'],
		'mac_address'=>$_POST['mac_address']
		],[
		'id_device'=>$_POST['id_device']
		]);
	
}



//Delete device
if(isset($_POST['device-del'])){

	$database->delete('device',['id_device'=>$_POST['id_device']]);

}

?>