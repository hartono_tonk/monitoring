<?php

// List & View barang
$barang=$database->select('barang',[
	'[><]status'=>'id_status',
	'[><]ruangan'=>'id_ruangan',
	],[
	'barang.id_barang',
	'barang.id_bluetooth',
	'barang.nama_barang',
	'barang.merek_barang',
	'barang.nomor_inventaris',
	'status.id_status',
	'status.nama_status',
	'ruangan.id_ruangan',
	'ruangan.nama_ruangan',
	]);


$total_inventaris=$database->count('barang');
$inventaris_aktif=$database->count('barang',['id_status'=>1]);
$inventaris_perbaikan=$database->count('barang',['id_status'=>2]);
$inventaris_hilang=$database->count('barang',['id_status'=>3]);
$inventaris_rusak=$database->count('barang',['id_status'=>4]);

//barang dalam setatus aktif
$status_aktif=$database->select('barang',[
	'[><]status'=>'id_status',
	'[><]ruangan'=>'id_ruangan',
	],[
	'barang.id_barang',
	'barang.id_bluetooth',
	'barang.nama_barang',
	'barang.merek_barang',
	'barang.nomor_inventaris',
	'status.id_status',
	'status.nama_status',
	'ruangan.id_ruangan',
	'ruangan.nama_ruangan'
	],[
	'nama_status'=>'Aktif'
	]);

//barang dalam status perbaikan
$status_perbaikan=$database->select('barang',[
	'[><]status'=>'id_status'
	],[
	'barang.id_barang',
	'barang.id_bluetooth',
	'barang.nama_barang',
	'barang.merek_barang',
	'barang.nomor_inventaris',
	'status.id_status',
	'status.nama_status'
	],[
	'nama_status'=>'Dalam Perbaikan'
	]);

//barang dalam status rusak
$status_rusak=$database->select('barang',[
	'[><]status'=>'id_status'
	],[
	'barang.id_barang',
	'barang.id_bluetooth',
	'barang.nama_barang',
	'barang.merek_barang',
	'barang.nomor_inventaris',
	'status.id_status',
	'status.nama_status'
	],[
	'nama_status'=>'Rusak'
	]);

//barang dalam hilang
$status_hilang=$database->select('barang',[
	'[><]status'=>'id_status'
	],[
	'barang.id_barang',
	'barang.id_bluetooth',
	'barang.nama_barang',
	'barang.merek_barang',
	'barang.nomor_inventaris',
	'status.id_status',
	'status.nama_status'
	],[
	'nama_status'=>'Hilang'
	]);


//tambah barang
if(isset($_POST['barang-add'])){

	$database->insert('barang',[
		'nama_barang'=>$_POST['nama_barang'],
		'merek_barang'=>$_POST['merek_barang'],
		'id_bluetooth'=>$_POST['id_bluetooth'],
		'id_ruangan'=>$_POST['id_ruangan'],
		'nomor_inventaris'=>$_POST['nomor_inventaris'],
		'id_status'=>$_POST['id_status']
		]);
}

//Update barang
if(isset($_POST['barang-update'])){
// print_r($_POST);
	$database->update('barang',[
		'nama_barang'=>$_POST['nama_barang'],
		'merek_barang'=>$_POST['merek_barang'],
		'id_bluetooth'=>$_POST['id_bluetooth'],
		'id_ruangan'=>$_POST['id_ruangan'],
		'nomor_inventaris'=>$_POST['nomor_inventaris'],
		'id_status'=>$_POST['id_status']
		],[
		'id_barang'=>$_POST['id_barang']
		]);
}

// barang get id
if(!empty($_GET['barang'])) {

	$barang_view=$database->get('barang',[
		'[><]status'=>'id_status'
		],[
		'barang.id_barang',
		'barang.id_bluetooth',
		'barang.id_ruangan',
		'barang.nama_barang',
		'barang.merek_barang',
		'barang.nomor_inventaris',
		'status.id_status',
		'status.nama_status'
		],[
		'id_barang'=>$_GET['barang']
		]);
}



//Delete barang
if(isset($_POST['barang-del'])){

	$database->delete('barang',['id_barang'=>$_POST['id_barang']]);

}

//nama ruangan
$ruangan=$database->select('ruangan','*');

//nama status
$status=$database->select('status','*');




$conn = mysqli_connect('localhost', 'root', '', 'monitoring_db');

$barangList = mysqli_query($conn, 'SELECT * FROM barang INNER JOIN ruangan ON ruangan.id_ruangan = barang.id_ruangan INNER JOIN status ON status.id_status = barang.id_status');
$idx = 0;
$res = [];
while($row = mysqli_fetch_assoc($barangList)){

	$mon = mysqli_query($conn, 'SELECT * FROM monitoring WHERE id_barang=' . $row['id_barang'] . ' ORDER BY id_monitoring DESC LIMIT 10');

	$mon_idx = 0;
	$not_found = 0;
	while ($f = mysqli_fetch_assoc($mon)) {
		if (!$f['found']){
			$not_found++;
		}
		$mon_idx++;
	}

	$row['not_found'] = $not_found;
	$res[$idx] = $row;
	$idx++;
}




?>