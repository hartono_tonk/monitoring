import requests
import json
from bluepy.btle import Scanner

ip_server = "http://localhost/monitoring"
barang_url = "/api/barang.php"
monitoring_url = "/api/monitoring.php"
header= {'Content-type': 'application/json'}

barangs = []
id_bluetooth_src = []
id_bluetooth_found = []
id_bluetooth_new = []
id_bluetooth_not_found = []
max_radius = '-10'
id_ruangan = 2

def getBarang():
    x = requests.post(ip_server + barang_url, json={'id_ruangan':id_ruangan, 'get-barang': 'iya'})
    x.raise_for_status()
    data = x.json()
    barangs = data['data']
    i = 0
    for barang in barangs: 
        id_bluetooth_src.append(barang['id_bluetooth'])
        i+=1
    
def scanBluetooth():
    scanner = Scanner()
    devices = scanner.scan(10.0)

    for dev in devices:
        if dev.addr in id_bluetooth_src:
            id_bluetooth_found.append(dev.addr)
        else:
            id_bluetooth_new.append(dev.addr)
    print (dev)
    for b_src in id_bluetooth_src:
        if b_src not in id_bluetooth_found:
            id_bluetooth_not_found.append(b_src)

def sendDataToServer():
    post_data = {}
    post_data['id_ruangan'] = id_ruangan
    post_data['found'] = id_bluetooth_found
    post_data['not_found'] = id_bluetooth_not_found
    post_data['new'] = id_bluetooth_new
    post_data['set_monitoring'] = 'iya dooong'
    post_data_json = json.dumps(post_data)

    x = requests.post(ip_server + monitoring_url, data=post_data_json, headers=header) 
    x.raise_for_status()
    print("done")    
        
getBarang()
scanBluetooth()
sendDataToServer()